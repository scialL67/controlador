#!/usr/bin/python
""" Script para probar pasaje de argumentos al ejecutar un comando """

import sys
import argparse
import serial
from command import formatCommandString

SERIAL = "/dev/ttyACM0"

def parse_args():
    parser = argparse.ArgumentParser(description="Script para probar pasaje de argumentos al ejecutar un comando")
    parser.add_argument('pos1', type=str, help="Argumento posicional 1")
    parser.add_argument('pos2', type=str, help="Argumento posicional 2")
    parser.add_argument('--opc1', type=str, help="Argumento opcional 1")
    parser.add_argument('--opc2', type=str, help="Argumento opcional 2")
    parser.add_argument('--leer', action='store_true', help="Quedar leyendo lo que recibe del puerto")
    return parser.parse_args()

def main():
    args = parse_args()
    ser = serial.Serial(SERIAL, 115200, timeout=1)
    print(f'Conectado al puerto serial {SERIAL}')
    command = 'ej_arg'
    command_string = formatCommandString(command, args, exclude_args=['leer'])
    try:
        ser.write(command_string.encode())
        print(ser.readline().decode().strip())
        while args.leer:
            result = ser.readline().decode().strip()
            if result == "":
                break
            else:
                print(result)
    except KeyboardInterrupt:
        if ser != None:
            print("\nKeyboard Interrupt. Cerrando puerto.")
            ser.close()
    ser.close()

if __name__=='__main__':
    sys.exit(main())
