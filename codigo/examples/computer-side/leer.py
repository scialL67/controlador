#!/usr/bin/python
""" Script para leer lo que se printea en la pico """

import sys
import argparse
import serial
from command import formatCommandString

SERIAL = "/dev/ttyACM0"

def main():
    ser = serial.Serial(SERIAL, 115200)
    print(f'Conectado al puerto serial {SERIAL}')
    try:
        while True:
            result = ser.readline().decode().strip()
            if result == "":
                break
            else:
                print(result)
    except KeyboardInterrupt:
        if ser != None:
            print("\nKeyboard Interrupt. Cerrando puerto.")
            ser.close()
    print("Listo. Cerrando puerto.")
    ser.close()

if __name__=='__main__':
    sys.exit(main())
