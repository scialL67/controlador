#!/usr/bin/python
""" Script para probar setear atributos de objetos """

import sys
import argparse
import serial
from command import formatCommandString

SERIAL = "/dev/ttyACM0"

def parse_args():
    parser = argparse.ArgumentParser(description="Script para probar setear atributos de objetos")
    parser.add_argument('texto', type=str, help="String que será seteado como atributo del objeto")
    parser.add_argument('--leer', action='store_true', help="quedar leyendo lo que recibe del puerto")
    return parser.parse_args()

def main():
    args = parse_args()
    ser = serial.Serial(SERIAL, 115200, timeout=1)
    print(f'Conectado al puerto serial {SERIAL}')
    command_string = formatCommandString('ej_atr', args, exclude_args=['leer'])
    try:
        ser.write(command_string.encode())
        print(ser.readline().decode().strip())
        while args.leer:
            result = ser.readline().decode().strip()
            if result == "":
                break
            else:
                print(result)
    except KeyboardInterrupt:
        if ser != None:
            print("\nKeyboard Interrupt. Cerrando puerto.")
            ser.close()
    ser.close()

if __name__=='__main__':
    sys.exit(main())
