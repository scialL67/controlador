def formatCommandString(command, args, exclude_args=[]):
    """ Función que arma el string formateado para mandarlo al microcontrolador.
    'command' es el nombre del comando a ejecutar.
    'args' el objeto que devuelve parse_args() con los argumentos y sus valores.
    'exclude_args' es una lista con el nombre de los argumentos que uno no quiere que se envíen."""
    dict_args = vars(args).copy() # copy porque de lo contrario me lo saca también de args
    for excluded_arg in exclude_args:
        dict_args.pop(excluded_arg)
    list_args = list(dict_args.keys())
    list_values = list(dict_args.values())
    command_string = f'{command};'
    for i in range(len(list_args)):
        command_string += f'{list_args[i]}:{list_values[i]},'
    if command_string[-1] == ',': # si no lleva argumentos por default no tiene una coma al final, y entonces lo que termina sacando es el ';', lo cual causa un error en el getCommandAndArgs
        command_string = command_string[:-1] # le saco la última ','. Ya sé.
    command_string += '\n'
    return command_string
