import utime as time

class example_class():
    def __init__(self, i=0, j=0, texto='default'):
        self.i = i
        self.j = j
        self.texto = texto

    def ejemplo_loop(self, otro_texto=''):
        """ Método de ejemplo para probar ponerlo en el loop """
        if self.i%self.j == 0:
            print(self.texto, self.i, otro_texto)
        self.i += 1

    def ejemplo_argumentos(self, pos1, pos2, opc1='default 1', opc2='default 2'):
        """ Método de ejemplo para probar pasar argumentos posicionales y opcionales """
        print('posicional 1: ', pos1)
        print('posicional 2: ', pos2)
        print('opcional 1: ', opc1)
        print('opcional 2: ', opc2)

    def ejemplo_atributo(self, texto):
        """ Método de ejemplo para cambiar el valor de algún atributo de la clase """
        self.texto = texto
