import sys
import _thread
import commander
from commander import getCommandAndArgs
import examplelib
import utime as time
import select

###
def readInput():
    #  print("Reading started")
    poll = select.poll()
    poll.register(sys.stdin, select.POLLIN)
    while True:
        #  print("Reading looped")
        out = poll.poll(1000) # 1000ms es el timeout
        if len(out) != 0:
            fileIO = out[0][0]
            try:
                command, args_dic = getCommandAndArgs(fileIO.readline().strip())
            except:
                print("Exception while unpacking command and arguments, continuing.")
                continue
            #  print(f'Recieved {command} with {args_dic}')
            if command in cmdr.commands:
                cmdr.queue_add(command, args_dic)
            else:
                print(f'Invalid command: {command}')
        out = []
        command = ""
        time.sleep_ms(500)

def main():
    _thread.start_new_thread(readInput, [])
    while True:
        time.sleep(1)
        #  print('New queue/loop iteration')
        try:
            cmdr.queue_execute()
            cmdr.loop_execute()
        except:
            print("Exception on queue/loop iteration")
            continue

if __name__ == "__main__":
    # Inicializo el commander
    cmdr = commander.commander()
    # Creo el objeto y le paso sus atributos
    example_obj = examplelib.example_class(j=3, texto='hola')

    # Agrego los métodos del objeto que quiero usar a la lista de comandos, dándoles un nombre. Notar que los métodos se pasan sin (); por ejemplo: example_obj.ejemplo_loop y no example_obj.ejemplo_loop().
    cmdr.command_add('ej_loop', example_obj.ejemplo_loop)
    cmdr.command_add('ej_arg', example_obj.ejemplo_argumentos)
    cmdr.command_add('ej_atr', example_obj.ejemplo_atributo)

    # Agrego el comando 'ej_loop' al loop. Esto significa que ese método se va a ejecutar periódicamente (cada 1s en este caso, según el sleep que hay en main()). Puedo pasar argumentos al método al agregarlo al loop.
    cmdr.loop_add('ej_loop', otro_texto='Japish!')
    try:
        main()
    except:
        sys.exit()
