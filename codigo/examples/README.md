# Ejemplos
Algunos ejemplos de uso. **Las versiones de `commander.py` y `main.py` que se usan acá estan desactualizadas**, pero sirven para el propósito de estos ejemplos.

## `Commander` y agregar comandos
En la librería `examplelib.py` hay una clase con métodos para ejecutar. Para hacerlo primero debemos inicializar el `commander`, que es una clase que se encarga de administrar los comandos que se quieren ejecutar. Éstos tienen que ser agregados al mismo, dándoles un nombre para su uso:
```python
cmdr = commander.commander() # Inicializamos el commander
example_obj = examplelib.example_class(j=3, texto='hola') # Inicializamos el objeto de la librería de ejemplos

cmdr.command_add('ej_arg', example_obj.ejemplo_argumentos) # Agregamos los métodos, dándoles un nombre
cmdr.command_add('ej_atr', example_obj.ejemplo_atributo)
```

## Ejecutar comandos
Ahora estos comandos serán ejecutados si el microprocesador recibe el string con el comando `ej_arg` o `ej_atr` (seguidos por sus argumentos cuando corresponda) respectivamente. Es importante notar que al momento de agregar el método, debemos escribir, por ejemplo, `example_obj.ejemplo_argumentos` y no `example_obj.ejemplo_argumentos()`.

Le comunicamos al microprocesador qué comando ejecutar y con qué argumentos enviándole un string formateado, por ejemplo, de la siguiente forma: `comando;arg1:valor1,arg2:valor2\n` para ejecutar el comando `comando` con los argumentos `arg1 = valor1` y `arg2 = valor2`. El último `\n` es lo que señaliza que es el final del mensaje.

Veamos, como ejemplo, cómo decirle al microprocesador que ejecute el método `example_obj.ejemplo_argumentos` que definimos por el comando `ej_arg`. Éste método sólo printea el valor de sus argumentos. Hay dos argumentos posicionales (obligatorios) y dos opcionales:
```python
def ejemplo_argumentos(self, pos1, pos2, opc1='default 1', opc2='default 2'):
	print('posicional 1: ', pos1)
	print('posicional 2: ', pos2)
	print('opcional 1: ', opc1)
	print('opcional 2: ', opc2)
```

Si quisiesemos ejecutarlo con `pos1 = hola`, `pos2 = mundo` y `opc1 = 42` tendríamos que enviarle al microprocesador el siguiente string: `ej_arg;pos1:hola,pos2:mundo,opc1:42\n`. En vez de hacer eso a mano, podemos hacerlo ejecutando el script `ejemplo_argumentos.py` de la siguiente manera:
```shell
ejemplo_argumentos.py hola mundo --opc1 42 --leer
```
con lo cual el microprocesador respondería :
```shell
posicional 1: hola
posicional 2: mundo
opcional 1: 42
opcional 2: default 2
```
Notar que además le pasamos al script el argumento `--leer`. Éste le indica que espere la respuesta del microprocesador y printee su resultado. De lo contrario el método aún se ejecutaría, pero no veríamos su resultado.

No hace falta pasar a mano el string con el comando y sus argumentos, si miramos en `ejemplo_argumentos.py`, luego de definir el argument parser, podemos formatear automáticamente el string utilizando la función `formatCommandString`:
```python
args = parse_args() # parseamos los argumentos
command = 'ej_arg' # definimos el nombre del comando que queremos enviar
command_string = formatCommandString(command, args, exclude_args=['leer'])
```
en la última línea le pasamos a `formatCommandString` el nombre del comando, los argumentos del script (que a su vez se pasarán al microprocesador) y una lista de argumentos que queremos excluir. Esto último es útil para no envíar argumentos ___locales___, que deben afectar únicamente el comportamiento del script, pero que no necesariamente es un argumento válido para el método que queremos ejecutar en el microprocesador, como es en este caso con `leer`.

## El loop
Cuando en vez de querer ejecutar un comando una vez y listo, queremos que se ejecute un método periódicamente, debemos agregarlo al loop:
```python
cmdr.command_add('ej_loop', example_obj.ejemplo_loop) # defino el comando al igual que antes
cmdr.loop_add('ej_loop', otro_texto='Japish!') # lo agrego al loop. Ahora se ejecutará periódicamente
```
si vemos en qué consiste este método:
```python
def ejemplo_loop(self, otro_texto=''):
	if self.i%self.j == 0:
		print(self.texto, self.i, otro_texto)
	self.i += 1
```
vemos que sólo printea una cada `self.j` iteraciones las variables `self.texto`, `self.i` y la variable opcional `otro_texto` que se puede definir al momento de agregarla al loop, como vimos anteriormente. Luego de printear aumenta el valor de `self.i` en 1.

Como agregamos este método al loop en el `main()`, podemos ver, utilizando el script `leer.py` que se está ejecutando:
```shell
hola 6 Japish!
hola 9 Japish!
hola 12 Japish!
hola 15 Japish!
```
como pusimos `j = 3` se printea cada 3 iteraciones.

Si agregásemos varios comandos al loop, digamos
```python
cmdr.command_add('cmd1', método1)
cmdr.command_add('cmd2', método2)
cmdr.loop_add('cmd1')
cmdr.loop_add('cmd2')
```
ambos se ejecutarían periódicamente y en el orden en el cual fueron agregados al mismo.

Esta no es la única forma de agregar un método al loop, sino que también lo podemos hacer desde un script externo. Hay dos comandos `loop_add` y `loop_remove` ya definidos en el `commander`, que al llamarlos, nos permiten agregar/quitar un comando al/del loop (siempre y cuando el comando que queremos agregar ya haya sido agregado al `commander` utilizando `command_add`). El script `ejemplo_loop.py` hace esto mismo. Si hicieramos
```shell
ejemplo_loop.py remove ej_loop
```
podríamos observar, mediante `leer.py`, que ya no se está ejecutando. Podríamos también volver a agregarlo haciendo
```shell
ejemplo_loop.py add ej_loop
```
