import utime as time

class PWM():
    """ Out-of-the-box PWM from machine module is for short time periods,
        and the typical action time of the valve is much longer. """

    def __init__(self, system):
        self.system = system
        self.t0 = time.time()
        # ^ reference time in order to bypass losing floating point precision
        # when calculating time_ratio

    def pwm(self):
        value = 0
        time_ratio = (time.time()
                - self.t0)/self.system.parameters['pwm_period']
        #  print('time_ratio', time_ratio)
        #  print('fraccion', time_ratio - int(time_ratio))
        if (time_ratio - int(time_ratio)
                >= self.system.parameters['pwm_duty']):
            value = 0
        else:
            value = 1
        return value

class valve_controller():
    def __init__(self, system, valve, pwm=None):
        self.system = system
        self.valve = valve
        self.pwm = pwm

    def basic_control(self):
        #  self.warning_rate() # comentado hasta que esté last_temperature en system
        if (self.system.variables['temperature']
                < self.system.parameters['T_target']
                - self.system.parameters['T_target_range']):
            self.valve.set_value(0)
        elif (self.system.variables['temperature']
                > self.system.parameters['T_target']
                + self.system.parameters['T_target_range']):
            self.valve.set_value(1)

    def warning_rate(self):
        dt = time.time() - self.system.last_time
        if dt > self.system.parameters['med_interval']*60:
            dT = (self.system.variables['temperature']
                    - self.system.variables['last_temperature'])
            rate = dT/self.system.parameters['med_interval']
            if abs(rate) > self.system.parameters['rate_limit']:
                print(f'ABSOLUTE TEMPERATURE CHANGE RATE {abs(rate)}K/min GREATER THAN LIMIT {self.system.parameters['rate_limit']}K/min !!')

    def pwm_control(self):
        value = self.pwm.pwm()
        #  print(value)
        self.valve.set_value(value)
