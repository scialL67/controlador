import time
import json

class System():
    def __init__(self, parameters: dict, variables: dict):
        self.variables = {v: None for v in variables.keys()}
        self._var_funcs = {v: f for v, f in variables.items()}
        self.parameters = parameters
        self.last_time = 0
        self.update_system()

    def update_system(self):
        for var in self.variables.keys():
            self.variables[var] = self._var_funcs[var](system=self)
        self.last_time = time.time()

    def query(self):
        query_dict = {'time':self.last_time,
                      'parameters':self.parameters,
                      'variables':self.variables
                      }
        print(json.dumps(query_dict))
        print('\n')

    def set(self, parameter: str, value: float):
        if parameter in self.parameters.keys():
            self.parameters[parameter] = value
        else:
            print('{parameter} is not a defined system parameter.')
