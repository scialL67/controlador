import _thread

class Commander():
    def __init__(self):
        self.commands = {'loop_add':self.loop_add, 'loop_remove':self.loop_remove} # diccionario de comandos y sus funciones correspondientes. Por default ya hay dos que sirven para agregar/quitar comandos al loop. Todo comando que se quiera ejecutar deberá estar en esta lista
        self.loop = [] # la lista de comandos que se encuentran en el loop. Sus funciones correspondientes se ejecutan periodicamente
        self._looplock = _thread.allocate_lock() # locks para que no se acceda a las listas desde dos threads distintos al mismo tiempo, lo podría causar conflictos
        self.queue = [] # comandos en el queue. (Lista de espera). Se ejecutan una vez.
        self._queuelock = _thread.allocate_lock()
        self._queuecap = 5 #  máxima cantidad de comandos en el queue

    def command_add(self, command, func):
        self.commands[command] = func

    def command_remove(self, command):
        self.commands.pop(command, None)

    def loop_add(self, command, **kwargs):
        if command in list(self.commands.keys()):
            if command not in [i[0] for i in self.loop]:
                with self._looplock:
                    self.loop.append((command, kwargs))
            else:
                print(f'Command {command} already in loop.')
        else:
            print(f'Command {command} is not a valid command.')

    def loop_remove(self, command):
        if command in [i[0] for i in self.loop]:
            with self._looplock:
                self.loop.remove([i for i in self.loop if i[0] == command][0])
        else:
            print(f'Command {command} is not in loop')

    def loop_execute(self): # ejecutar las funciones del loop while True
        for command, kwargs in self.loop:
            #  print(f'loop_execute {command} con {kwargs}')
            self.commands[command](**kwargs) # notar que no tienen argumentos (podría cambiarse)

    def queue_add(self, command, args_dic):
        if len(self.queue) + 1 <= self._queuecap: # si no se excedería del max, agregar
            with self._queuelock:
                self.queue.append((command, args_dic)) # el elemento del queue incluye el nombre del comando y los argumentos que se le quieran dar a la función
        else:
            print(f'Queue full with {len(self.queue)} items')

    def queue_remove(self, command):
        with self._queuelock:
            self.queue.remove([i for i in self.queue if i[0] == command][0]) # quitar de la lista el primer elemento cuyo comando sea command

    def queue_execute(self): # ejecutar todos los comandos que estén en espera
        for command, args_dic in self.queue:
            #  print(f'queue_execute {command} con {args_dic}')
            self.commands[command](**args_dic)
            self.queue_remove(command)

def getCommandAndArgs(input):
    """ Separa el comando de sus argumentos.
    El comando se encuentra a la izquierda de ';' y a su derecha los argumentos.
    Los argumentos están separados uno de otro por ','.
    El nombre del argumento y su valor están separados por ':' """
    command, args = input.split(';')
    args_dic = {}
    if not args == '': # Para que funcione en el caso de que no hayan argumentos
        args_split = args.split(',')
        for pair in args_split:
            key, value = pair.split(':')
            args_dic[key] = valueConvert(value)
    return command, args_dic

def valueConvert(string):
    """ Convertimos strings en floats si es posible hacer la conversión """
    try:
        out = float(string)
    except ValueError:
        out = string
    return out
