from machine import Pin
import utime as time

class valve():
    def __init__(self, pin=15, min_toggle_time=2000):
        self.control_pin = Pin(pin, Pin.OUT, value=0)
        self.min_toggle_time = min_toggle_time # tiempo mínimo entre toggleos en milisegundos
        self.last_time = time.ticks_ms()
        """ Acá este last_time es distinto al de sistema. """

    def allow_toggle(self):
        dt = time.ticks_diff(time.ticks_ms(), self.last_time)
        allow = dt > self.min_toggle_time
        return allow

    def toggle(self, loop_interval=0):
        """ loop_interval es el intervalo de tiempo que querés que pase entre
            cada toggle automático si es que ponés esta función
            en el loop, en ms """
        dt = time.ticks_diff(time.ticks_ms(), self.last_time)
        if self.allow_toggle() and (dt > loop_interval):
            self.control_pin.toggle()
            self.last_time = time.ticks_ms()
            #  print(f'control_pin set to {self.control_pin.value()}')

    def set_value(self, value):
        if self.allow_toggle():
            self.control_pin.value(value)
            self.last_time = time.ticks_ms()

    def valve_state(self, system):
        value = self.control_pin.value()
        return value
