import sys
import _thread
import utime as time
import select
import gc

import max31865_smith

import commander
from commander import getCommandAndArgs
import valve
import valve_control
import temperature
import system_status

###
def readInput():
    #  print("Reading started")
    poll = select.poll()
    poll.register(sys.stdin, select.POLLIN)
    while True:
        #  print("Reading looped")
        out = poll.poll(1000) # 1000ms es el timeout
        if len(out) != 0:
            fileIO = out[0][0]
            try:
                command, args_dic = getCommandAndArgs(fileIO.readline().strip())
            except:
                print("Exception while unpacking command and arguments, continuing.")
                continue
            # print(f'Recieved {command} with {args_dic}')
            if command in cmdr.commands:
                cmdr.queue_add(command, args_dic)
            else:
                print(f'Invalid command: {command}')
        out = []
        command = ""
        time.sleep_ms(500)
        gc.collect()

def main():
    _thread.start_new_thread(readInput, [])
    while True:
        time.sleep(1)
        #  print('New queue/loop iteration')
        try:
            cmdr.queue_execute()
            cmdr.loop_execute()
        except:
            print("Exception on queue/loop iteration")
            continue
        gc.collect()

if __name__ == "__main__":
    valve_obj = valve.valve(pin=15, min_toggle_time=2000)
    max31865 = temperature.max31865(clkPin=10, misoPin=11,
                                    mosiPin=12, csPin=13)

    parameters={
            'T_target':130, 'T_target_range':1, 'med_interval':5,
            'rate_limit':1,
            'pwm_duty':0, 'pwm_period':10
    }
    var_dict={
            'temperature':max31865.medirTemp,
            'valve_state':valve_obj.valve_state
    }

    syst = system_status.System(parameters, var_dict)
    pwm = valve_control.PWM(syst)
    controlador = valve_control.valve_controller(syst,
            valve=valve_obj, pwm=pwm)

    # Initialize commander and define commands
    cmdr = commander.Commander()
    cmdr.command_add('query', syst.query)
    cmdr.command_add('update_system', syst.update_system)
    cmdr.command_add('set', syst.set)

    cmdr.command_add('basic_control', controlador.basic_control)
    cmdr.command_add('pwm_control', controlador.pwm_control)

    cmdr.command_add('toggle_valve', valve_obj.toggle)

    cmdr.loop_add('update_system')
    cmdr.loop_add('basic_control')
    try:
        #  print("Executing main()")
        main()
    except:
        print("An exception ocurred.")
        sys.exit()
