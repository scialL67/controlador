#!/usr/bin/python
""" Script para testear agregar/borrar cosas al loop desde la pc """

import sys
import argparse
import serial
from command import formatCommandString

SERIAL = "/dev/ttyACM0"

def parse_args():
    parser = argparse.ArgumentParser(description=" Script para testear agregar/borrar cosas al loop desde la pc ")
    parser.add_argument('action', type=str, help='Acción a ejecutar (add/remove)', choices=['add', 'remove'])
    parser.add_argument('command', type=str, help='Comando a agregar o eliminar')
    return parser.parse_args()

def main():
    args = parse_args()
    ser = serial.Serial(SERIAL, 115200, timeout=1)
    print(f'Conectado al puerto serial {SERIAL}')
    command = f'loop_{args.action}'
    command_string = formatCommandString(command, args, exclude_args=['action'])
    command_string += f''
    try:
        ser.write(command_string.encode())
        print(ser.readline().decode().strip())
    except:
        print('Pasó algo malo cuando se mandaba el comando')
    print("Listo. Cerrando puerto.")
    ser.close()

if __name__=='__main__':
    sys.exit(main())
