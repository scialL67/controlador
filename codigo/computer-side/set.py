#!/usr/bin/python
"""Script para mandar comandos relacionados al funcionamiento de la válvula"""

import sys
import argparse
import serial
import time

SERIAL = "/dev/ttyACM0"

def parse_args():
    parser = argparse.ArgumentParser(
            description="Script for setting system parameter values.")
    parser.add_argument('parameter', type=str,
                        help="Parameter to set.")
    parser.add_argument('value', type=float,
                        help="Value for the parameter.")
    parser.add_argument('--read', action='store_true',
                       help="Read input from port.")
    return parser.parse_args()

def main():
    args = parse_args()
    ser = serial.Serial(SERIAL, 115200, timeout=1)
    print(f'Connected to serial port {SERIAL}')
    command_string = f'set;parameter:{args.parameter},value:{args.value}\n'
    try:
        ser.write(command_string.encode())
        print(ser.readline().decode().strip())
    except:
        print('Exception ocurred during writing')
    if args.read:
        try:
            while args.read:
                result = ser.readline().decode().strip()
                if result == "":
                    break
                else:
                    print(result)
        except KeyboardInterrupt:
            if ser != None:
                print("\nKeyboard Interrupt. Cerrando puerto.")
                ser.close()
    print("Done. Closing port.")
    ser.close()

if __name__=='__main__':
    sys.exit(main())
