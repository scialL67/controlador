#!/usr/bin/python
"""Script to send the valve toggle command"""

import sys
import argparse
import serial

SERIAL = "/dev/ttyACM0"

def main():
    ser = serial.Serial(SERIAL, 115200, timeout=1)
    print(f'Connected to serial port {SERIAL}.')
    command_string = f'toggle_valve;\n'
    try:
        ser.write(command_string.encode())
        print(ser.readline().decode().strip())
    except:
        print('Exception ocurred during writing.')
    print("Done. Closing port.")
    ser.close()

if __name__=='__main__':
    sys.exit(main())
