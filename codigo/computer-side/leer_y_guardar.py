#!/usr/bin/python
""" Script para leer y guardar lo que se printea en la pico """

import sys
import serial
import datetime

SERIAL = "/dev/ttyACM0"

def main():
    ser = serial.Serial(SERIAL, 115200)
    print(f'Conectado al puerto serial {SERIAL}')
    current_time = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    with open(f"mediciones/{current_time}.txt", 'w') as file:
        try:
            while True:
                result = ser.readline().decode().strip()
                print(result)
                file.write(result+'\n')
        except KeyboardInterrupt:
            if ser != None:
                print("\nKeyboard Interrupt. Cerrando puerto.")
                ser.close()
    ser.close()

if __name__=='__main__':
    sys.exit(main())
