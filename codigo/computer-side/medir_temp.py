#!/usr/bin/python
""" Script para leer y guardar para las meds de temperatura """

import sys
import argparse
import serial
import datetime
import time
import json


SERIAL = "/dev/ttyACM0"

def parse_args():
    parser = argparse.ArgumentParser(
            description="Script for reading and saving temperature data.")
    parser.add_argument('sleep', type=float,
            help="Time between readings in seconds. Preferably more than 5s.")
    return parser.parse_args()


def read_write(file, ser):
    result = ser.readline().decode().strip()
    if result != '':
        print(result)
        try:
            dic = json.loads(result)

            t = dic['time']
            temp = dic['variables']['temperature']
            vstate = dic['variables']['valve_state']
            duty = dic['parameters']['pwm_duty']

            file.write(f'{t};{temp};{vstate};{duty}\n')

        except json.decoder.JSONDecodeError:
            print(f'Received an invalid response: {result}. Continuing')


def add_query():
    command = f'loop_add;command:query\n'
    with serial.Serial(SERIAL, 115200, timeout=1) as ser:
        ser.write(command.encode())
        print(ser.readline().decode().strip())
    print('Query added to loop')


def remove_query():
    command = f'loop_remove;command:query\n'
    with serial.Serial(SERIAL, 115200, timeout=1) as ser:
        ser.write(command.encode())
        print(ser.readline().decode().strip())
    print('Query removed from loop')


def main():
    arg = parse_args()
    current_time = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    add_query()
    with open(f"mediciones/{current_time}.txt", 'w') as file:
        try:
            while True:
                with serial.Serial(SERIAL, 115200, timeout=1) as ser:
                    print(f'Connected to serial port {SERIAL}')
                    read_write(file, ser)
                    print(f'Closing connection to serial port {SERIAL}')
                time.sleep(arg.sleep)
        except KeyboardInterrupt:
            remove_query()
            print(f"\nInterrupted. Serial port {SERIAL} closed.")


if __name__=='__main__':
    sys.exit(main())
