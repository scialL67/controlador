## 20/04/22
-Corregido que faltaba un self: `queue_remove` -> `self.queue_remove` en `queue_execute`

-Decidimos que los comandos que se quieren ejecutar desde afuera son cosas que se corren instantáneamente, así que van a ser sync.

-Decidimos que no hace falta que `loop_add` y `loop_remove` necesiten locks: eso es algo que se hace en el mismo thread a pedido de un comando externo y forma sincrónica

## 02/05/22
-Agregamos otro script de prueba `test2.py` para probar pasar argumentos

-Agregamos una función `test2` en `testlib.py` para para probar lo mismo

-Agregamos una función `getCommandsAndArgs` en `main.py` que separa el comando y crea un diccionario con los argumentos para pasarlos a los métodos correspondientes

-Correspondientemente cambiamos `args` -> `args_dic` en `queue_add` y `queue_execute` dentro de `commander.py`

-Probamos cambiar una variable desde afuera. Para eso agregamos una `test3` en `testlib.py` que printea el nombre de una variable global `A` ubicada en `variables.py`. Ahí adentro está también una función `editarA` mediante la cual se edita el valor de aquella. Correspondientemente script `test3.py` en la computadora para mandar el comando de edición

## 03/05/22
-Solucionamos el problema de que la línea `input = sys.stdin.readline().strip()` bloquea, no usando el `StreamReader` de `uasyncio`, cuya funcionalidad sólo funciona para TCP, sino usando el objeto `poll` de la librería `select`. A efectos prácticos implementa un timeout a la lectura, con lo cual si no recibe nada por un tiempo aún deja que el resto del loop ejecute. Notar que aún hasta que se haga efectivo el timeout va a estar bloqueando

-Corregimos los argumentos de la función `test3` en `testlib.py` que los tenía mal

-Cambiamos el nombre de `variables.py` a `testvars.py`. Ahora el enfoque es en cambiar atributos de objetos, e hicimos cambios correspondientes

## 09/05/22
-Nuevo branch `separar-lectura-loop` donde hacemos eso mismo

-Como parte de esto fletamos todo lo que era async hasta que sea explícitamente necesario, eso implica cambios en `main.py`, `commander.py` y `testlib.py`

## 11/05/22
-Agregamos una libreria `commands.py` computer-side en la cual se encuentra una función `formatCommandString` para que automáticamente formatee, según el nombre del comando y sus argumentos, el string para comunicarlo al microprocesador. Editamos `test2.py` y `test3.py` correspondientemente

-Agregamos un script `leer.py` computer-side sólo para leer lo que se printea en el microprocesador (para debuggear/testear)

-Agregamos un script `loop_test.py` computer-side para probar agregar/quitar comandos del loop. Al mismo tiempo cambiamos el nombre del parámetro `name` en `commander.py` a `command`

-Agregamos la posibilidad de setear argumentos al agregar una función al loop. Modificamos el método `test1` en `testlib.py` para testearlo.

-Movimos `getCommandAndArgs` de `main.py` a `commander.py`

## 20/05/22
-Corregimos una fuente de errores en `formatCommandString` cuando no se envían argumentos para el comando.

-Agregamos un chequeo en `getCommandAndArgs` para el caso en el que no hayan argumentos para evitar un error

-Agregamos una libreria para controlar la válvula `valve.py` y en pc-side un script para mandar los respectivos comandos `toggle_valve.py`. Modificamos `main.py` acorde para hacer pruebas

## 24/05/22
-Nueva carpeta 'examples' con ejemplos y explicación.

-Eliminamos el código de prueba (aunque son la base de los ejemplos que agregamos): `testlib.py` y los scripts `test2.py` y `test3.py`. Nos quedamos con uno, renombrando `loop_test.py` a `loop.py` ya que este sí es de utilidad.

-Limpiamos los printeos de debug en `main.py` y `commander.py`.

-Agregamos un método en `valve.py` para query el estado de la válvula.

-Cambiamos el manejo del tiempo entre toggleos en `valve.py` para que no bloquee si aún no paso el tiempo requerido. Ahora en vez de un `sleep` simplemente hace nada.

-Agregamos librerias para medición de temperatura `max31865_smith`, que necesita a `gpio.py`. En `temperatura.py` extendemos el objeto definido en smith y definimos un método `medirTemp`.

-Agregamos una librería `control.py` en la cual se encuentra el método `valve_controller.py` que usaremos para controlar la válvula según la temperatura. Ahora sólo contiene un controlador básico ON-OFF `basic_control`.

-Agregamos el script computer-side `valve.py` con comandos para controlar la válvula. Eliminamos el script `toggle_valve.py` ya que su funcionalidad quedó dentro de `valve.py`

## 26/05/22
-Manejamos el caso en el que ocurra una excepción en `readInput()` para que no se pierda comunicación con el microprocesador en caso de que suceda.

## 01/06/22
-Pasamos los argumentos a floats en `getCommandAndArgs()`.

-Hecho lo anterior sacamos la conversión a float en `setT_target()` en `control.py`

## 02/06/22
-Sacamos el `query_state()` de `valve.py` en el microcontrolador a `control.py`. Ahora devuelve el estado de la válvula, la temperatura, la temperatura objetivo y el rango admisible.

-Cambiamos el nombre del script pc-side `valve.py` a `regulador.py` porque no es sano tener el mismo nombre en cosas distintas. También hicimos cambios para manejar mejor el uso del puerto.

-Ahora no se puede agregar un comando al loop si ya está presente en el mismo.

-Corregimos un error en `valueConvert()` en `commander.py`.

## 04/06/22
-Agregamos chequeos de si el comando es válido al querer agregarlo al loop mediante `loop_add`, y si el comando efectivamente está en el loop al intentar removerlo mediante `loop_remove`

-Manejamos el caso de que ocurra una excepción al querer ejecutar `queue_execute()` o `loop_execute()` en cada iteración del loop, para que si sucede no se detenga el microprocesador.

-Nos aseguramos que se cierre el puerto al finalizar los scripts

-Agregamos un timeout de 1s a la comunicación de los scripts con el microprocesador, y agregamos lectura de una línea luego de enviar el comando como solución temporal para el problema de que no se enviaban hasta la segunda comunicación.

## 07/06/22
-Agregamos advertencia si la tasa de cambio de la temperatura es mayor a 1K/m (por default). Movimos `medir_temp()` para que sea inicializado desde `basic_control()`, ya no hace falta agregarlo explícitamente al loop antes de iniciar el control.

-Cambiamos los tiempos a epoch

## 13/06/22
-Agregamos un garbage collector al final de cada loop en cada thread, ya que el problema que había que la Raspberry se colgaba después de un tiempo era debido a un memory leak.

## 08/07/22
-Cambiamos el nombre de la librería `control.py` a `valve_control.py` para que tenga nombre más descriptivo

-Agregamos la librería  `system_status.py`, que contiene todos los parámetros y variables del sistema, además de setters y updaters para los mismos

-Como consecuencia del uso de `system_status` sacamos todos los métodos en otras librerias cuya función pasa a estar en ésta. También editamos dichas librerías para que obtengan sus parámetros de `system_status`: `temperature.py`, `valve_control.py`, `valve.py`

-Cambiamos `main.py` para utilizar la nueva librería.

-Eliminamos `regulador.py`; separamos y expandimos sus funcionalidades en otro conjunto de scripts `set.py`, `query.py` y `valve_toggle.py`.

-Pusimos, de momento, que si aún no pasó el tiempo `med_interval`, que `medirTemp` devuelva la temperatura que ya está registrada. Es para evitar que devuelva `None`.

## 12/07/22
-Agregamos la clase `PWM` a `valve_control.py`, y el método `pwm_control()`.

-Agregamos método `set_value()` a `valve.py`, como alternativa a `toggle()`.

-Movimos el `allow_toggle()` de estar explícitamente en `valve_control.py` a estar dentro de `valve.py`

-Pusimos para que el script `set.py` cierre apropiadamente el puerto al finalizar.

## 14/07/22
-Los diccionarios de `system_status.py` ahora los parseamos con `json`.

-`valve_state()` ya no devuelve una aclaración "abierto/cerrado", sólo el valor del pin, porque era molesto.

-Script `medir_temp.py` para hacer las mediciones de temperatura.

## 19/07/22
-Cambiamos el script `medir_temp.py` para que no ocupe el puerto constantemente.

-Agregamos un chequeo en `System.set()` por si el parámetro no está definido.

-Algunos cambios de nombre (para respetar pep8):
	-El objeto `commander` -> `Commander`

-Nuevo readme
