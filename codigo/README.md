# `commander`
`commander` es una librería que aporta una clase, que se encarga de administrar los comandos que se quieren ejecutar. Lo primero es importar la librería e inicializar el objeto `Commander`:
```python
import commander
cmdr = commander.Commander() # Inicializamos el commander
```
## Comandos
### `command_add`
Antes de poder ejecutar una función preexistente, debemos asignarle un comando. Hacemos eso agregandola a la lista de comandos mediante `command_add`:
```python
cmdr.command_add('comando', funcion)
```
Como un ejemplo, en `main.py` tenemos, entre otros casos:
```python
cmdr.command_add('toggle_valve', valve_obj.toggle)
```
de ahora en más, cuando el microcontrolador reciba como input `toggle_valve` procederá a agregar el método `valve_obj.toggle` al _queue_, la cual es una lista de comandos en espera que serán ejecutados secuencialmente, en orden de llegada. En general, a menos que se envien muchos comandos al mismo tiempo, la queue estará vacía y la ejecución será inmediata.

## Loop
El _loop_ es una lista de comandos que se ejecutarán en cada ciclo del microcontrolador. Estos comandos se ejecutan secuencialmente en el orden en el que fueron agregados a la misma.

### `loop_add` y `loop_remove`
La lista es estática, a menos que se la altere mediante los comandos `loop_add` y `loop_remove` que se encuentran ya definidos al tiempo de inicialización del `Commander`.

Como ejemplo, en `main.py` agregamos al loop los siguientes comandos, que ya habíamos agregado mediante `command_add` previamente:
```python
cmdr.loop_add('update_system')
cmdr.loop_add('basic_control')
```

Por otro lado, `loop_remove` quita del loop un comando que ya se encuentre agregado. A este comando normalmente nos interesará invocarlo desde fuera del microcontrolador, asunto que trataremos más adelante.

# `system_status`
Es una librería que se encarga de recopilar todos los parámetros y mantener actualizadas las variables del sistema. Hay que importar la librería e inicializar el objeto `System`:
```python
import system_status
syst = system_status.System(parameters, var_dict)
```
Vemos que toma dos argumentos, `parameters` y `var_dict` que describimos a continuación.

## `parameters`
Es un diccionario en donde definimos los nombres de todos los parámetros del sistema y sus valores por defecto con los que los inicializamos.

Tomemos como ejemplo los parámetros que definimos en `main.py`, relacionados al control de temperatura mediante la válvula:
```python
parameters={
	'T_target':130, 'T_target_range':1, 'med_interval':5,
	'rate_limit':1,
	'pwm_duty':0, 'pwm_period':10
}
```

Podemos acceder a esos parámetros mediante el atributo `parameters` del objeto `System`, de la forma usual al tratarse de un diccionario:
```shell
>>> syst.parameters['T_target']
130
```

## `variables`
El otro argumento que toma `System` al ser inicializado es `var_dict`. Éste es también un diccionario, pero en vez de pasarle el nombre de la variable y un valor por defecto, el diccionario contiene los nombres de las variables y los respectivos métodos que las actualizan.

Un ejemplo de variable es la temperatura del sistema. En `main.py` vemos:
```python
var_dict={
	'temperature':max31865.medirTemp,
	'valve_state':valve_obj.valve_state
}
```
es decir, definimos como variable del sistema `temperature`, y su valor será actualizado en cada ciclo del microcontrolador mediante el método `medirTemp` del objeto `max31865`, que inicializamos anteriormente.

Hay dos condiciones que debe cumplir un método que actualiza una variable:
-	El método debe retornar **únicamente** el nuevo valor de la variable
-	El método **debe** tomar como **único** argumento `system`, que es la instancia de la clase `System`
Como ejemplo, veamos el codigo de `medirTemp`:
```python
def medirTemp(self, system):
	dt = time.time() - system.last_time
	if dt >= system.parameters['med_interval']:
		res, temp = self.getTemp()
		temp += 273.15
	else:
		temp = system.variables['temperature']
	return temp
```
Incluso si el método no usa explícitamente el objeto, debe cumplir la segunda condición. Un ejemplo de esto es `valve_state`, del objeto `valve_obj`:
```python
def valve_state(self, system):
	value = self.control_pin.value()
	return value
```

Análogo a los parámetros, en cualquier momento se puede acceder al valor actual de una variable mediante el atributo `variables` del objeto `System`, que es también un diccionario. Si en la última medición de temperatura se obtuvo 1337K, luego:
```shell
>>> syst.variables['temperature']
1337
```

Además de los atributos `parameters` y `variables`, es posible consultar el tiempo en el cual fue hecha la última actualización de las variables, mediante el atributo `last_time` del objeto `System`. Éste tiempo es el epoch.

## `query`
`System` posee el método `query`, el cual al ser llamado devuelve un diccionario que contiene toda la información del sistema: los atributos `time`, `parameters` y `variables` mencionados anteriormente. La salida está formateada en json.

## `set`
También posee el método `set`, con el cual es posible modificar los valores de los parámetros del sistema ya definidos. Normalmente lo invocaremos desde fuera del microcontrolador.

# Comunicación desde la computadora
## Formato de comunicación
En general, si queremos ejecutar un comando `comando` ya definido en el `Commander` (como vimos antes) con ciertos argumentos `arg1 = valor1`, `arg2 = valor2`, debemos enviar al microcontrolador un `string` formateado de la siguiente forma: `comando;arg1:valor1,arg2:valor2\n`. Es decir, separamos `comando` de los argumentos mediante una `;`, a cada argumento lo separamos de su valor con `:`, y a distintos argumentos los separamos entre sí con `,`. El último `\n` señaliza el final del mensaje.

No hace falta pasar a mano el string con el comando y sus argumentos, podemos formatear automáticamente el string utilizando la función `formatCommandString`. Para ver ejemplos de esto, miren la carpeta examples.

## Algunos scripts ya definidos
Pueden encontrar algunos scripts ya escritos, relacionados a comandos mencionados anteriormente. Estos son:
-	`query.py`: le pasa al microcontrolador el comando `query`, y printea lo que devuelve, que es el diccionario con la información del sistema mencionado anteriormente.
-	`set.py`: le pasa al microcontrolador el comando `set`, y lleva como argumentos qué parámetro setear y qué valor asignarle, por ejemplo: `./set.py T_target 130`.
-	`loop.py`: recibe dos argumentos `add` o `remove`, y el nombre del comando. Por ejemplo: `./loop.py add basic_control` para añadir ese comando al loop, o `./loop.py remove basic_control` para quitarlo.
-	`leer.py`: lee continuamente lo que llegue del microcontrolador y lo printea. Notar que si se recibe un final de línea `\n` la comunicación se detendrá.
